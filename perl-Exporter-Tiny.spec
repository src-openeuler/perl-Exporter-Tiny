Name:           perl-Exporter-Tiny
Version:        1.006002
Release:        2
Summary:        An exporter with the features of Sub::Exporter but only core dependencies
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Exporter-Tiny
Source0:        https://cpan.metacpan.org/modules/by-module/Exporter/Exporter-Tiny-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  coreutils findutils make
BuildRequires:  perl-generators perl-interpreter
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.17
BuildRequires:  perl(Carp) perl(strict) perl(warnings)
BuildRequires:  perl(lib) perl(Test::More) >= 0.47
Requires:       perl(Carp)

%{?perl_default_filter}

%description
Exporter::Tiny supports many of Sub::Exporter's external-facing features
including renaming imported functions with the -as, -prefix and -suffix
options; explicit destinations with the into option; and alternative
installers with the installer option. But it's written in only about 40%
as many lines of code and with zero non-core dependencies.
Its internal-facing interface is closer to Exporter.pm, with configuration
done through the @EXPORT, @EXPORT_OK and %%EXPORT_TAGS package variables.

%package_help

%prep
%autosetup -n Exporter-Tiny-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1
%make_build

%install
%make_install
%{_fixperms} -c %{buildroot}

%check
make test

%files
%doc CREDITS examples/ README
%license COPYRIGHT LICENSE
%{perl_vendorlib}/Exporter/

%files help
%doc Changes TODO
%{_mandir}/man3/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 1.006002-2
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Jul 25 2023 xujing <xujing125@huawei.com> - 1.006002-1
- update version to 1.006002

* Fri Oct 21 2022 xujing <xujing125@huawei.com> - 1.004004-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 1.004004

* Thu Jul 23 2020 xinghe <xinghe1@huawei.com> - 1.002002-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 1.002002

* Sat Oct 26 2019 shenyangyang <shenyangyang4@huawei.com> - 1.002001-5
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete unneeded build requires of perl(Test::Warnings)

* Tue Oct 15 2019 shenyangyang <shenyangyang4@huawei.com> - 1.002001-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete unneeded build requires

* Sun Sep 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.002001-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: revise requires and change the directory of some files 

* Sat Sep 14 2019 zhangsaisai <zhangsaisai@huawei.com> - 1.002001-2
- Package init
